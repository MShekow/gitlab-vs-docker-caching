# GitLab vs. Docker Caching

Illustrates for a NodeJS project how using Docker's build cache is significantly faster than using GitLab's CI/CD
caching. See
[this blog post](https://www.augmentedmind.de/2022/06/12/gitlab-vs-docker-caching-pipelines/) for
background information.

Uses a NodeJS sample app from [here](https://github.com/async-labs/builderbook/tree/master/builderbook).

Refer to the branches `docker-caching` and `gitlab-caching` to see the respective CI/CD pipeline definitions.

**Note: to run the `docker-caching` version, you need a (static) runner that has access to a Docker daemon socket!**
